const { NODE_ENV: env } = process.env

module.exports = {
  sourceMaps: env === 'production' ? false : 'inline',
  plugins: [
    ['@babel/transform-typescript', {
      allowNamespace: false,
      optimizeConstEnum: true,
      allowDeclareFields: true,
      disallowAmbiguousJSXLike: true
    }]
  ]
}
