# [Docker Exercise](https://docs.google.com/presentation/d/e/2PACX-1vRcQn_F2ilPbn0kyDfp-blCQ_UhAU0Z3B1tvtQgqPP6IyL91W7tB0mAolh3RTY5Y6Eq5T5oeeIk6RNH/pub?start=false&loop=false&delayms=60000)

Try your hand running a node server on docker

## Install Docker
```bash
# enable https repository
sudo apt-get update
sudo apt-get install \
  ca-certificates \
  curl \
  gnupg \
  lsb-release

# gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# set stable repository
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
  https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# install docker package
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# enable without sudo, requires logout
sudo groupadd docker
sudo usermod -aG docker $USER
```
