import Koa from 'koa'

import port from './config/port.js'

const koa = new Koa

koa.use(ctx => ctx.body = 'You did it!!!')
koa.listen(port, () => console.log(`listen(${port})`))
