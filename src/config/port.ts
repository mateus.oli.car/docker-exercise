import raise from '../util/raise.js'

export default +process.env.NODE_PORT! || raise('missing process.env.NODE_PORT')
